from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('index125/', views.index125, name='index125'),
    path('index500/', views.index500, name='index500'),
    path('index1000/', views.index1000, name='index1000')
]
