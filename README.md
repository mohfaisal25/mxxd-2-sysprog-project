# mxxd-2-sysprog-project

## Ringkasan
Pada aplikasi ini ada 2 script utama, yaitu hitierpolls.sh dan lotierpolls.sh. Keduanya melakukan hal yang sama tetapi hitierpolls bisa menerima input sesuai perintah user, sedangkan lotierpolls tidak.

Keduanya berguna untuk mengubah polling rate dari mouse yang menggunakan driver usbhid. lotierpolls akan mengubah polling rate menjadi 8 ms atau 125H, sedangkan hitierpolls menerima 1 argumen dalam bentuk ms untuk menjadi polling ratenya.

Script ini didukung oleh web interface yang memiliki beberapa preset button untuk menjalankan script tersebut.

## Lotierpolls
Untuk menggunakan Lotierpolls sama seperti cara memanggil bash script pada umumnya.
```
./lotierpolls.sh
```

## Hitierpolls
Untuk menggunakan Hitierpolls sama seperti lotier hanya saja ditambah dengan 1 argumen integer yang menandakan seberapa lama jangka waktu antar poll terjadi.
```
./hitierpolls.sh 8
```

## Web Interface
Web interface pendukung ini dibuat dengan framework django dan harus dijalankan dalam mode super user. Lalu path pada ./home/views.py perlu disesuaikan dengan path tempat hitierpolls berada.

```
sudo su
//asumsi sudah memiliki django dan python
python3 manage.py runserver
```

